package main

import (
	"fmt"
	as "github.com/aerospike/aerospike-client-go"
	log "github.com/sirupsen/logrus"
	"math"
	"os"
	"strconv"
	"strings"
)

type Configuration struct {
	Repos             []string `default:"127.0.0.1:3000"`
}

type MyJsonName struct {
	Timestamp   string `json:"TIMESTAMP"`
	Description string `json:"description"`
	ID          int64  `json:"id"`
	Image       string `json:"image"`
	Mdn         string `json:"mdn"`
	Name        string `json:"name"`
	Physical    bool   `json:"physical"`
	Season      int64  `json:"season"`
	VoucherID   int64  `json:"voucherId"`
}

var rec *as.Record

func InitRepository(repos []string) *as.Client {

	hosts := make([]*as.Host, 0)

	for _, host := range repos {
		params := strings.Split(host, ":")
		if port, e := strconv.Atoi(params[1]); e != nil {
			continue
		} else {
			hosts = append(hosts, as.NewHost(params[0], port))
		}
	}

	dbRepo, e := as.NewClientWithPolicyAndHost(nil, hosts...)

	if e != nil {
		log.Errorf("Unable to initialize repository %v", e)
		os.Exit(1)
	}

	return dbRepo
}

func main() {

	var Config Configuration
	fmt.Println(Config.Repos)
	Config.Repos = []string{"127.0.0.1:3000"}

	dbRepo := InitRepository(Config.Repos)

	keyString := fmt.Sprintf("%v","6288905695428_2020-12-08_2021-12-17_100287")

	key, _ := as.NewKey("knox", "/realme/prize", keyString)

	/*rObj := &MyJsonName{}

	if err := dbRepo.GetObject(nil, key, rObj); err != nil {
		fmt.Println(err)
	}*/

	var e error

	if rec, e = dbRepo.Get(nil, key, "data"); e != nil {

		fmt.Println(e)
	}

	fmt.Println(rec.Bins["data"].(string))

	if rec, e = dbRepo.Get(nil, key, "imei"); e != nil {

		fmt.Println(e)
	}

	fmt.Println(rec.Bins["imei"].(string))


	keyString = fmt.Sprintf("%v","6288905695428_2020-12-08_2021-12-17_100288")
	key, e = as.NewKey("knox", "/realme/prize", keyString)

	if e != nil {
		fmt.Println("Failed creating aero key")
		return
	}

	writePolicy := as.NewWritePolicy(0, math.MaxUint32)
	writePolicy.SendKey = true

	// Write multiple values.
	bin1 := as.NewBin("imei", "3534810900577777")
	bin2 := as.NewBin("data", "{\"mdn\":\"6288905695420\",\"id\":235,\"voucherId\":2913380,\"name\":\"Realme Band\",\"description\":\"UAT Realme\",\"image\":\"https://sp-stg1-web.smartfren.com/images/23d4a5b4be179c3ab5f0efc2e94b834b.png\",\"physical\":true,\"season\":1,\"TIMESTAMP\":\"2021-04-23 10:44:14\"}")

	dbRepo.PutBins(writePolicy, key, bin1, bin2)

}
